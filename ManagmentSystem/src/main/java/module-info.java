module org.managment.system.managmentsystem {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires mongo.java.driver;
    requires com.google.gson;
    requires json;

    opens org.managment.system.managmentsystem to javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view to javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view.login to javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view.homepage to javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view.registeruser to javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view.resetpassword to javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view.updatedocument to  javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.view.notification to  javafx.fxml,mongo.java.driver,javafx.base;
    opens org.managment.system.managmentsystem.mongodb.model to com.google.gson;
    opens org.managment.system.managmentsystem.mongodb.imports to com.google.gson;
    exports org.managment.system.managmentsystem;
}