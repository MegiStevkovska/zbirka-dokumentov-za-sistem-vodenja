package org.managment.system.managmentsystem.view.login;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;
import org.managment.system.managmentsystem.ManagmentSystemController;
import org.managment.system.managmentsystem.ManagmentSystemApplication;
import org.managment.system.managmentsystem.mongodb.model.User;
import org.managment.system.managmentsystem.mongodb.service.DBFinder;
import org.managment.system.managmentsystem.view.homepage.HomePageController;
import org.managment.system.managmentsystem.view.resetpassword.ResetPasswordController;

import java.io.IOException;


public class LoginController {

    @FXML
    TextField username, password;
    @FXML
    Label forgotenPassword;
    @FXML
    AnchorPane root;
    @FXML
    Button errorMessage;
    private Stage primaryStage;
    private Scene scene = null;

    public LoginController(Stage stage) {
        this.primaryStage = stage;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                root.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
                    if (ev.getCode() == KeyCode.ENTER) {
                        login(username.getText(), password.getText());
                    }
                });
            }
        });
    }

    @FXML
    public void onLoginButtonClick() {
        login(username.getText(), password.getText());
    }
    @FXML
    public void onMouseClicked(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("reset-password.fxml"));
        fxmlLoader.setController(new ResetPasswordController(primaryStage,username.getText()));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 600, 400);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(400);
        primaryStage.setMaxWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setTitle("Reset password");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    @FXML
    public void onKeyRealeased(KeyEvent keyEvent){
        TextField username = (TextField) keyEvent.getSource();
        User u = DBFinder.findUserInDB(username.getText());
        if(u != null){
            forgotenPassword.setVisible(true);
        }else{
            forgotenPassword.setVisible(false);
        }
    }

    @FXML
    public void onHomeClicked(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("home-page.fxml"));
        fxmlLoader.setController(new HomePageController(primaryStage));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 1366, 700);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setMinHeight(700);
        primaryStage.setMinWidth(1366);
        primaryStage.setMaxHeight(700);
        primaryStage.setMaxWidth(1366);
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private void login(String userename, String password) {
        errorMessage.setVisible(false);
        User user = DBFinder.findUserInDB(userename);
        if (user == null) {
            errorMessage.setVisible(true);
        } else {
            if (password != null && (password.equals(user.getPassword()))) {
                ManagmentSystemApplication.user = user.getUserName();
                FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("managment-system.fxml"));
                fxmlLoader.setController(new ManagmentSystemController(primaryStage));
                try {
                    scene = new Scene(fxmlLoader.load(), 1366, 700);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        primaryStage.setMinWidth(1366);
                        primaryStage.setMinHeight(700);
                        primaryStage.setMaxWidth(1366);
                        primaryStage.setMaxHeight(700);
                        primaryStage.setTitle("ManagmentSystem");
                        primaryStage.setScene(scene);
                        primaryStage.show();
                        primaryStage.sizeToScene();
                        primaryStage.setMaximized(true);
                        primaryStage.setResizable(false);
                    }
                });

            } else {
                errorMessage.setVisible(true);
            }
        }
    }
}
