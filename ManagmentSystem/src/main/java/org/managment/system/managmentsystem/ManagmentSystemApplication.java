package org.managment.system.managmentsystem;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;
import org.managment.system.managmentsystem.mongodb.MongoDB;
import org.managment.system.managmentsystem.view.homepage.HomePageController;
import org.managment.system.managmentsystem.view.login.LoginController;


public class ManagmentSystemApplication extends Application {

    public final static MongoDB databaseConnection = MongoDB.getInstance();
    public static String user = null;

    @Override
    public void start(Stage primaryStage) throws Exception {                   //(1)
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("home-page.fxml"));
        fxmlLoader.setController(new HomePageController(primaryStage));
        Scene scene = new Scene(fxmlLoader.load(), 1366, 700);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}