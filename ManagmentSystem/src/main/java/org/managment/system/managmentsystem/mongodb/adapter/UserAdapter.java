package org.managment.system.managmentsystem.mongodb.adapter;

import com.google.gson.*;
import org.managment.system.managmentsystem.mongodb.model.User;

public class UserAdapter implements JsonDeserializer<User> {
    public User deserialize(JsonElement json,
                            java.lang.reflect.Type typeOfT, JsonDeserializationContext
                                        context) throws JsonParseException {
        JsonObject jObj = json.getAsJsonObject();
        //String id =
        //        jObj.get("_id").toString().replaceAll(".*\"(\\w+)\"}",
        //                "$1");
        String username = jObj.get("username") != null ?
                jObj.get("username").getAsString() : "";
        String password = jObj.get("password")!= null ?
                jObj.get("password").getAsString() : "";
        String companyName = jObj.get("companyName")!= null ?
                jObj.get("companyName").getAsString() : "";
        String workEmail = jObj.get("workEmail")!= null ?
                jObj.get("workEmail").getAsString() : "";
        String securityQuestion = jObj.get("securityQuestion")!= null ?
                jObj.get("securityQuestion").getAsString() : "";
        String securityAnswer = jObj.get("securityAnswer")!= null ?
                jObj.get("securityAnswer").getAsString() : "";
       // int age = jObj.get("age")!= null ? jObj.get("age").getAsInt()
       //         : 0;
        return new User(username,password,companyName,workEmail,securityQuestion,securityAnswer);
    }
}
