package org.managment.system.managmentsystem.mongodb.adapter;

import com.google.gson.*;
import org.managment.system.managmentsystem.mongodb.model.Request;

public class RequestAdapter implements JsonDeserializer<Request> {
    public Request deserialize(JsonElement json,
                            java.lang.reflect.Type typeOfT, JsonDeserializationContext
                                        context) throws JsonParseException {
        JsonObject jObj = json.getAsJsonObject();
        //String id =
        //        jObj.get("_id").toString().replaceAll(".*\"(\\w+)\"}",
        //                "$1");
        String name = jObj.get("name") != null ?
                jObj.get("name").getAsString() : "";
        //String password = jObj.get("password")!= null ?
        //        jObj.get("password").getAsString() : "";
       // String phone = jObj.get("phone")!= null ?
        //        jObj.get("phone").getAsString() : "";
       // int age = jObj.get("age")!= null ? jObj.get("age").getAsInt()
       //         : 0;
        return new Request(name);
    }
}
