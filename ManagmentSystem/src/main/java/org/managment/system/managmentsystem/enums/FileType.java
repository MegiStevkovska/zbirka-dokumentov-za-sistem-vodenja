package org.managment.system.managmentsystem.enums;

public enum FileType {
     TXT,DOC,DOCX,XLS,XLSX,PDF;
}
