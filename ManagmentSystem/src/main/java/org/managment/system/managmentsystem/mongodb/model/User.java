package org.managment.system.managmentsystem.mongodb.model;

import com.google.gson.annotations.SerializedName;

public class User {

    public User(String userName, String password, String companyName, String workEmail, String securityQuestion, String securityAnswer) {
        this.userName = userName;
        this.password = password;
        this.companyName = companyName;
        this.workEmail = workEmail;
        this.securityQuestion = securityQuestion;
        this.securityAnswer = securityAnswer;
    }

    @SerializedName("username")
    private String userName;

    private String password;

    private String companyName;

    private String workEmail;

    private String securityQuestion;

    private String securityAnswer;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }
}
