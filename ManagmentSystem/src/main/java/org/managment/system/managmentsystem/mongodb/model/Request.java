package org.managment.system.managmentsystem.mongodb.model;

public class Request {

    public Request(String name){
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
