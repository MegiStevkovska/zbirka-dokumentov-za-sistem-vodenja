package org.managment.system.managmentsystem.view.homepage;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;
import org.managment.system.managmentsystem.ManagmentSystemApplication;
import org.managment.system.managmentsystem.RequestWithDocumentController;
import org.managment.system.managmentsystem.mongodb.service.DBImporter;
import org.managment.system.managmentsystem.view.login.LoginController;
import org.managment.system.managmentsystem.view.registeruser.RegisterUserController;

import java.io.File;
import java.io.IOException;

public class HomePageController {

    private Stage primaryStage;

    public HomePageController(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    ImageView message;

    @FXML
    public void onLoginButtonClick(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("login.fxml"));
        fxmlLoader.setController(new LoginController(primaryStage));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 600, 400);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(400);
        primaryStage.setMaxWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(false);
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }
    @FXML
    public void onImportTemplateButtonClick(){
        message.setImage(null);
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterTxt = new FileChooser.ExtensionFilter("json files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilterTxt);
        File f = fileChooser.showOpenDialog(primaryStage);
        if(f!= null && f.getPath().contains("user")){
            try {
                DBImporter.importUsers(f);
                message.setImage(new Image(RequestWithDocumentController.class.getResource("images/ok.png").toExternalForm()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }else if(f.getPath().contains("request")){
            try {
                DBImporter.importRequests(f);
                message.setImage(new Image(RequestWithDocumentController.class.getResource("images/ok.png").toExternalForm()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }else if(f.getPath().contains("document")){
            try {
                DBImporter.importDocumnets(f);
                message.setImage(new Image(RequestWithDocumentController.class.getResource("images/ok.png").toExternalForm()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }else{
            message.setImage(new Image(RequestWithDocumentController.class.getResource("images/error.png").toExternalForm()));
        }
            }




    @FXML
    public void onLRegisterButtonClick(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("register-user.fxml"));
        fxmlLoader.setController(new RegisterUserController(primaryStage));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 1366, 700);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setMinWidth(1366);
        primaryStage.setMinHeight(700);
        primaryStage.setMaxWidth(1366);
        primaryStage.setMaxHeight(700);
        primaryStage.setTitle("Register User");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.show();
    }



}
