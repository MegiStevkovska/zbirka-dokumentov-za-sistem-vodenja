package org.managment.system.managmentsystem.mongodb.service;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static org.managment.system.managmentsystem.mongodb.MongoDB.connection;

public class DBUpdater {

    public static void updateRequest(String oldName,String newName){
        MongoCollection coll = connection.getCollection("request");
        coll.updateOne(eq("name", oldName), new Document("$set", new
                Document("name", newName)));

    }

    public static void updateUserPassword(String userName,String newPassword){
        MongoCollection coll = connection.getCollection("user");
        coll.updateOne(eq("username", userName), new Document("$set", new
                Document("password", newPassword)));

    }

    public static void updateDocumentRequest(String name, List<String> requests){
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("requests", requests)));

    }

    public static void updateDocumentDate(String name, LocalDateTime date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm:ss");
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("date", formatter.format(date))));

    }

    public static void updateDocumentChangeJustification(String name, String changeJustification){
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("changeJustification", changeJustification)));

    }

    public static void updateDocumentUser(String name, String user){
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("user", user)));

    }

    public static void updateDocumentVersion(String name, String version){
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("version", version)));

    }

    public static void updateDocumentNotification(String name, String notification){
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("notification", notification)));

    }

    public static void updateDocumentNotificationDate(String name, LocalDateTime notificationDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm:ss");
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("notificationDate", formatter.format(notificationDate))));

    }

    public static void updateDocumentIsNotificationShown(String name, boolean isSelected){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm:ss");
        MongoCollection coll = connection.getCollection("documents");
        coll.updateOne(eq("name", name), new Document("$set", new
                Document("isNotificationShown", String.valueOf(isSelected))));

    }




}
