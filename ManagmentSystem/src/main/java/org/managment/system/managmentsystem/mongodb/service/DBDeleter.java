package org.managment.system.managmentsystem.mongodb.service;

import com.mongodb.client.MongoCollection;

import static com.mongodb.client.model.Filters.eq;
import static org.managment.system.managmentsystem.mongodb.MongoDB.connection;

public class DBDeleter {

    public static void deleteRequest(String name){
        MongoCollection coll = connection.getCollection("request");
        coll.deleteOne(eq("name", name));
    }

    public static void deleteDocument(String documentName){
        MongoCollection coll = connection.getCollection("documents");
        coll.deleteOne(eq("name", documentName));
    }


}
