package org.managment.system.managmentsystem;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;
import org.managment.system.managmentsystem.enums.Action;
import org.managment.system.managmentsystem.enums.ChoserType;
import org.managment.system.managmentsystem.enums.FileType;
import org.managment.system.managmentsystem.mongodb.model.Document;
import org.managment.system.managmentsystem.mongodb.service.DBDeleter;
import org.managment.system.managmentsystem.mongodb.service.DBFinder;
import org.managment.system.managmentsystem.mongodb.service.DBInserter;
import org.managment.system.managmentsystem.mongodb.service.DBUpdater;
import org.managment.system.managmentsystem.view.login.LoginController;
import org.managment.system.managmentsystem.view.notification.NotificationController;
import org.managment.system.managmentsystem.view.registeruser.RegisterUserController;
import org.managment.system.managmentsystem.view.updatedocument.UpdateDocumentController;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class RequestWithDocumentController {

    @FXML
    private Label name,version,date,user,title,errorMessage;
    @FXML
    TextArea changeJustification;
    @FXML
    GridPane gridPane;
    @FXML
    Button buttonEdit,buttonDelete;
    @FXML
    DatePicker dataPicker;
    private Stage primaryStage;
    private List<AnchorPane> anchorPaneList = new ArrayList<>();

    private Button selectedButton = null;
    private TextField mouseEntered =  null;
    private String titleFromRequests;
    Scene scene =null;
    private Action action = null;
    private Stage updateStage = null;

    @FXML
    private AnchorPane  anchor10, anchor20, anchor30, anchor40, anchor50, anchor60, anchor11, anchor21, anchor31, anchor41, anchor51, anchor61,
            anchor12, anchor22, anchor32, anchor42, anchor52, anchor62;


    public RequestWithDocumentController(Stage primaryStage,String titleFromRequests1){
        this.primaryStage = primaryStage;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                titleFromRequests = titleFromRequests1;
                anchorPaneList.addAll(Arrays.asList(anchor10, anchor20, anchor30, anchor40, anchor50, anchor60, anchor11, anchor21, anchor31, anchor41, anchor51, anchor61,
                        anchor12, anchor22, anchor32, anchor42, anchor52, anchor62));
                setGlobalEventHandler();
                setTextFieldEventHandler();
                title.setText(title.getText() + titleFromRequests);
                gridPane.setOnDragOver(new EventHandler<DragEvent>()  {
                    public void handle(DragEvent event) {
                        if (event.getDragboard().hasFiles()) {
                            event.acceptTransferModes(TransferMode.ANY);
                        }
                        event.consume();
                    }
                });
                gridPane.setOnDragDropped(new EventHandler<DragEvent>() {
                    public void handle(DragEvent event) {
                        List<File> files = event.getDragboard().getFiles();
                        System.out.println("Got " + files.size() + " files");
                        File f = files.get(0);
                        showFielOnForm(f);
                        Document document = setDataAboutFile(createFileDataOnNewEvent(f));
                        DBInserter.insertNewDocument(document);
                       // imageView.setImage(new Image(new FileInputStream(files.get(0))));

                        event.consume();
                    }
                });
                Scene scene = primaryStage.getScene();
                scene.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        for (AnchorPane ap : anchorPaneList) {
                            List<Node> element = ap.getChildren();
                            for (Node n : element) {
                                if (n instanceof TextField) {
                                    TextField tf = (TextField) n;
                                    if(tf.isFocused() && !tf.equals(mouseEntered)){
                                        tf.setDisable(true);
                                    }
                                }
                            }
                        }
                    }
                });
                List<Document> documents = DBFinder.listDocuments();
                if(documents.size()>0) {
                    for (Document document : documents) {
                        if(document.getRequests().contains(titleFromRequests1)){
                        AnchorPane temp = null;
                        for (AnchorPane ap : anchorPaneList) {
                            if (!ap.isVisible()) {
                                ap.setVisible(true);
                                temp = ap;
                                break;
                            }
                        }
                        List<Node> element = temp.getChildren();
                        for (Node n : element) {
                            if (n instanceof TextField) {
                                TextField tf = (TextField) n;
                                tf.setText(document.getName());
                                tf.setDisable(true);
                                break;
                            }else if(n instanceof Button){
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        Button b = (Button) n;
                                        ImageView iv = (ImageView) b.getChildrenUnmodifiable().get(0);
                                        Image image = new Image(RequestWithDocumentController.class.getResource(getPathImageFromFileType(getPathImageFromFileName(document.getName()),isShowNotification(document.getName()))).toExternalForm());
                                        iv.setImage(image);
                                        b.requestFocus();
                                        selectedButton = b;
                                    }
                                });


                            }
                        }
                    }
                        setDataAboutFile(document);
                    }
                }




            }
        });
    }

    @FXML
    protected void onAddDocumentButtonClick() {
        action = Action.INSERT;
        File file = createFielChoser(ChoserType.OPEN);
        if(file != null && !"".equals(file.getName())) {
                showFielOnForm(file);
                Document document1 =setDataAboutFile(createFileDataOnNewEvent(file));
                DBInserter.insertNewDocument(document1);
        }
    }
    @FXML
    protected void onEditDocumentButtonClick(ActionEvent actionEvent) {
        action = Action.UPDATE;
        AnchorPane ap = (AnchorPane)selectedButton.getParent();
        List<Node> nodeList= ap.getChildren();
        TextField temp = null;
        for(Node n:nodeList){
            if(n instanceof TextField){
                temp = (TextField) n;
            }
        }
        updateStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("update-document.fxml"));
        fxmlLoader.setController(new UpdateDocumentController(updateStage,temp.getText(),this));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 600, 500);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        updateStage.setMinWidth(600);
        updateStage.setMinHeight(500);
        updateStage.setMaxWidth(600);
        updateStage.setMaxHeight(500);
        updateStage.setTitle("Update document");
        updateStage.setScene(scene);
        //primaryStage.setMaximized(true);
        updateStage.setResizable(false);
        updateStage.show();


    }
    @FXML
    protected void onDeleteDocumentButtonClick() {
        AnchorPane ap = (AnchorPane) selectedButton.getParent();
        ap.setVisible(false);
        List<Node> element = ap.getChildren();
        for (Node n : element) {
            if (n instanceof TextField) {
                TextField tf = (TextField) n;
                tf.setDisable(false);
                DBDeleter.deleteDocument(tf.getText());
                tf.setText("");
                user.setText("");
                changeJustification.setText("");
                version.setText("");
                name.setText("");
                date.setText("");
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tf.requestFocus();
                    }
                });
                break;
            }
        }
        for (int i = anchorPaneList.size()-1;i>=0;i--) {
            AnchorPane ap2 =anchorPaneList.get(i);
            if (ap2.isVisible()) {
                List<Node> element2 = ap2.getChildren();
                for (Node n : element2) {
                    if (n instanceof Button) {
                        Button b = (Button) n;
                        b.requestFocus();
                        selectedButton = b;
                    }
                }
                break;
            }
        }
    }

    @FXML
    public void onMouseClicked(ActionEvent actionEvent){
        selectedButton = (Button) actionEvent.getSource();
    }

    @FXML
    public void onMouseEntered(MouseEvent e)
    {
        TextField tf  = (TextField) e.getSource();
        mouseEntered = tf;
    }
    @FXML
    public void onMouseExited(MouseEvent e)
    {
        mouseEntered = null;
    }

    @FXML
    public void onRequestButtonCliced(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("managment-system.fxml"));
        fxmlLoader.setController(new ManagmentSystemController(primaryStage));
        try {
            scene = new Scene(fxmlLoader.load(), 1366, 700);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                primaryStage.setMinWidth(1366);
                primaryStage.setMinHeight(700);
                primaryStage.setMaxWidth(1366);
                primaryStage.setMaxHeight(700);
                primaryStage.setTitle("ManagmentSystem");
                primaryStage.setScene(scene);
                primaryStage.show();
                // primaryStage.centerOnScreen();
                primaryStage.setMaximized(true);
                primaryStage.setResizable(false);
            }
        });
    }

    @FXML
    public void onNotificationButtonClick(){
        AnchorPane ap = (AnchorPane)selectedButton.getParent();
        List<Node> nodeList= ap.getChildren();
        TextField temp = null;
        for(Node n:nodeList){
            if(n instanceof TextField){
                temp = (TextField) n;
            }
        }
        Stage notification = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("notification-document.fxml"));
        fxmlLoader.setController(new NotificationController(notification,temp.getText(),this));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 600, 500);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        notification.setMinWidth(600);
        notification.setMinHeight(500);
        notification.setMaxWidth(600);
        notification.setMaxHeight(500);
        notification.setTitle("Notification");
        notification.setScene(scene);
        //primaryStage.setMaximized(true);
        notification.setResizable(false);
        notification.show();
    }

    private FileType getPathImageFromFileName(String fileName){
        if(fileName == null || fileName.contains("txt")){
            return FileType.TXT;
        }else if(fileName.contains("doc")){
            return FileType.DOC;
        }else if(fileName.contains("docx")){
            return FileType.DOCX;
        }else if(fileName.contains("xls")){
            return FileType.XLS;
        }else if(fileName.contains("xlsx")){
            return FileType.XLSX;
        }else if(fileName.contains("pdf")){
            return FileType.PDF;
        }
        return FileType.TXT;
    }
    private String getPathImageFromFileType(FileType fileType,boolean isShowNotification){
        switch (fileType){
            case DOC:
            case DOCX:
                return isShowNotification?"images/word-notification.jpg":"images/word.jpg";
            case XLS:
            case XLSX:
                return isShowNotification?"images/xls-notification":"images/xls.png";
            case PDF:
                return isShowNotification?"images/pdf-noticification.png":"images/pdf.png";
            default:{
                return isShowNotification?"images/txt-notification.jpg":"images/txt.jpg";
            }
        }
    }

    private void showFielOnForm(File file ){
        for (AnchorPane ap : anchorPaneList) {
            Document document = DBFinder.findDocumentByName(file.getName());
            if(document != null){
                if(action == Action.INSERT) {
                    errorMessage.setText("Document alredy exist in database !");
                    action = null;
                }
                return;
            }else{
                if(action == Action.UPDATE || action == Action.DELETE) {
                    errorMessage.setText("Document not exist in database !");
                    action = null;
                    return;
                }
            }
            if (!ap.isVisible()) {
                ap.setVisible(true);
                List<Node> element = ap.getChildren();
                for (Node n : element) {
                    if (n instanceof TextField) {
                        TextField tf = (TextField) n;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                tf.setDisable(true);
                                tf.setText(file.getName());
                            }
                        });
                    } else if (n instanceof Button) {

                            Button b = (Button) n;
                            ImageView iv = (ImageView) b.getChildrenUnmodifiable().get(0);
                            Image image = new Image(RequestWithDocumentController.class.getResource(getPathImageFromFileType(getPathImageFromFileName(file.getName()),isShowNotification(document.getName()))).toExternalForm());
                            iv.setImage(image);
                            b.requestFocus();
                            selectedButton = b;
                    }
                }
                break;
            }
        }
    }
    public Document setDataAboutFile(Document document){
        if(document != null) {
            name.setText(document.getName());
            version.setText(document.getVersion());
            date.setText(document.getDate());
            user.setText(document.getUser());
            changeJustification.setText(document.getChangeJustification());
            document.setRequests(new ArrayList<String>());
            document.getRequests().add(titleFromRequests);
        }
        return document;
    }

    private Document createFileDataOnNewEvent(File file){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm:ss");
        Document document = new Document(file.getName(),"1.0.0",formatter.format(LocalDateTime.now()),ManagmentSystemApplication.user,"New Document",null,null,null,new ArrayList<String>(),"false");
        return document;
    }

    private void setTextFieldEventHandler() {
        for (AnchorPane ap : anchorPaneList) {
            List<Node> element = ap.getChildren();
            for (Node n : element) {
                if (n instanceof TextField) {
                    TextField tf = (TextField) n;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            tf.focusedProperty().addListener(new ChangeListener<Boolean>()
                            {
                                @Override
                                public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
                                {
                                    if (!newPropertyValue)
                                    {
                                        tf.setDisable(true);
                                    }
                                }
                            });
                        }
                    });
                }else if (n instanceof Button) {
                    Button b = (Button) n;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            b.focusedProperty().addListener(new ChangeListener<Boolean>() {
                                @Override
                                public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                                    if (!newPropertyValue) {
                                        selectedButton = b;
                                    }else{
                                        AnchorPane ap = (AnchorPane) b.getParent();
                                        List<Node> nodeList = ap.getChildren();
                                        for(Node n : nodeList){
                                            if(n instanceof TextField){
                                                TextField tf =(TextField) n;
                                                Document document = DBFinder.findDocumentByName(tf.getText());
                                                setDataAboutFile(document);
                                            }
                                        }
                                    }
                                }
                            });
                            b.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent mouseEvent) {
                                    if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                                        if(mouseEvent.getClickCount() == 2){
                                            System.out.println("Dva klika");
                                            FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("request-with-document.fxml"));
                                            //fxmlLoader.setController(new RequestWithDocumentController(primaryStage,getFolderName(mouseEvent)));
                                            Scene scene = null;
                                            try {
                                                scene = new Scene(fxmlLoader.load(), 1366, 700);
                                            } catch (IOException e) {
                                                throw new RuntimeException(e);
                                            }
                                            scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
                                            //       primaryStage.setTitle(getFolderName(mouseEvent));
                                            primaryStage.setScene(scene);
                                            primaryStage.setMaximized(false);
                                            primaryStage.setResizable(false);
                                            primaryStage.show();
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            }
        }
    }
    private void setGlobalEventHandler() {
        gridPane.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                for (AnchorPane ap : anchorPaneList) {
                    List<Node> element = ap.getChildren();
                    for (Node n : element) {
                        if (n instanceof TextField) {
                            TextField tf = (TextField) n;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    if(tf.isFocused()) {
                                        tf.setDisable(true);
                                        return;
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    private File createFielChoser(ChoserType choserType){
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterTxt = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        FileChooser.ExtensionFilter extFilterXls = new FileChooser.ExtensionFilter("Excel 97-2003 (*.xls)", "*.xls");
        FileChooser.ExtensionFilter extFilterXlsx = new FileChooser.ExtensionFilter("Excel Workbook (*.xlsx)", "*.xlsx");
        FileChooser.ExtensionFilter extFilterDoc = new FileChooser.ExtensionFilter("Word 97-2003 Document (*.doc)", "*.doc");
        FileChooser.ExtensionFilter extFilterDocx = new FileChooser.ExtensionFilter("Word Document (*.docx)", "*.docx");
        FileChooser.ExtensionFilter extFilterPdf = new FileChooser.ExtensionFilter("PDF Files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilterTxt);
        fileChooser.getExtensionFilters().add(extFilterXls);
        fileChooser.getExtensionFilters().add(extFilterXlsx);
        fileChooser.getExtensionFilters().add(extFilterDoc);
        fileChooser.getExtensionFilters().add(extFilterDocx);
        fileChooser.getExtensionFilters().add(extFilterPdf);
        File f = null;
        switch (choserType){
            case SAVE -> f = fileChooser.showSaveDialog(primaryStage);
            default ->  f = fileChooser.showOpenDialog(primaryStage);
        }
        return  f;
    }

    @FXML
    public void onLogout(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("login.fxml"));
        fxmlLoader.setController(new LoginController(primaryStage));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 600, 400);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(400);
        primaryStage.setMaxWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(false);
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    public void update(String changeJustification,String documentName){
        updateStage.hide();
        DBUpdater.updateDocumentDate(documentName,LocalDateTime.now());
        DBUpdater.updateDocumentUser(documentName,ManagmentSystemApplication.user);
        DBUpdater.updateDocumentChangeJustification(documentName,changeJustification);
        DBUpdater.updateDocumentVersion(documentName,upDocumentVersion(documentName));
    }

    private String upDocumentVersion(String documentName){
        Document document =DBFinder.findDocumentByName(documentName);
        String version = "V1.0.0";
        if(document.getVersion() != null && !"".equals(document.getVersion())) {
            version = document.getVersion().split("\\.")[0];
            int v = Integer.valueOf(version.replace("V", ""));
            return "V" + (++v) + ".0.0";
        }
        else
            return version;
    }
private boolean isShowNotification(String name){
       Document document = DBFinder.findDocumentByName(name);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    LocalDateTime dateTime = LocalDateTime.parse(document.getDate(),formatter);
    LocalDateTime dateTimeNotification = LocalDateTime.parse(document.getDate(),formatter);
    if(dateTimeNotification.isBefore(LocalDateTime.now()) && !Boolean.valueOf(document.getIsNotificationShown())){
        return true;
    }
    return false;
}
}
