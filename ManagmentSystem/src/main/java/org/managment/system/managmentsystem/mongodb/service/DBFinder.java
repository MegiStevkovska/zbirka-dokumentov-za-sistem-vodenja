package org.managment.system.managmentsystem.mongodb.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.managment.system.managmentsystem.mongodb.adapter.DocumentsAdapter;
import org.managment.system.managmentsystem.mongodb.adapter.RequestAdapter;
import org.managment.system.managmentsystem.mongodb.adapter.UserAdapter;
import org.managment.system.managmentsystem.mongodb.model.Request;
import org.managment.system.managmentsystem.mongodb.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static org.managment.system.managmentsystem.mongodb.MongoDB.connection;

public class DBFinder {

    public static User findUserInDB(String userName) {
        MongoCollection coll = connection.getCollection("user");
        GsonBuilder builder = new GsonBuilder();
        Gson gson = new GsonBuilder().registerTypeAdapter(User.class, new
                UserAdapter()).create();

        return gson.fromJson(gson.toJson(coll.find(eq("username", userName)).first()), User.class);
    }

    public static List<String> listRequests(){
        List<String> requestList = new ArrayList<>();
        MongoCollection coll = connection.getCollection("request");
        MongoCursor<Document> cursor = coll.find().iterator();
        Gson gson = new GsonBuilder().registerTypeAdapter(Request.class, new
                RequestAdapter()).create();

        try {
            while (cursor.hasNext()) {
                Document doc = cursor.next();
                Request r =gson.fromJson(gson.toJson(doc), Request.class);
                requestList.add(r.getName());
            }
        } finally {
            cursor.close();
        }
        return requestList;
    }

    public static List<org.managment.system.managmentsystem.mongodb.model.Document> listDocuments(){
        List<org.managment.system.managmentsystem.mongodb.model.Document> documentList = new ArrayList<>();
        MongoCollection coll = connection.getCollection("documents");
        MongoCursor<Document> cursor = coll.find().iterator();
        Gson gson = new GsonBuilder().registerTypeAdapter(Document.class, new
                DocumentsAdapter()).create();

        try {
            while (cursor.hasNext()) {
                Document doc = cursor.next();
                org.managment.system.managmentsystem.mongodb.model.Document document =gson.fromJson(gson.toJson(doc),  org.managment.system.managmentsystem.mongodb.model.Document.class);
                documentList.add(document);
            }
        } finally {
            cursor.close();
        }
        return documentList;
    }

    public static org.managment.system.managmentsystem.mongodb.model.Document  findDocumentByName(String name){
        MongoCollection coll = connection.getCollection("documents");
        Gson gson = new GsonBuilder().registerTypeAdapter(org.managment.system.managmentsystem.mongodb.model.Document.class, new
                DocumentsAdapter()).create();

        return gson.fromJson(gson.toJson(coll.find(eq("name", name)).first()), org.managment.system.managmentsystem.mongodb.model.Document.class);
    }

}
