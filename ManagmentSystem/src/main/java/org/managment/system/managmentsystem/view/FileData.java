package org.managment.system.managmentsystem.view;

import java.time.LocalDateTime;

public class FileData {
    private String name;
    private String version;
    private String user;
    private LocalDateTime date;
    private String changeJustification;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getChangeJustification() {
        return changeJustification;
    }

    public void setChangeJustification(String changeJustification) {
        this.changeJustification = changeJustification;
    }
}
