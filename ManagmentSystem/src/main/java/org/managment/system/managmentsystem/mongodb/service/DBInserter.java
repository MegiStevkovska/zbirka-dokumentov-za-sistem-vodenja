package org.managment.system.managmentsystem.mongodb.service;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import static org.managment.system.managmentsystem.mongodb.MongoDB.connection;

public class DBInserter {

    public static void insertNewRequest(String name){
        MongoCollection coll = connection.getCollection("request");
       // Gson gson = new Gson();
 Document doc = new Document("name", name);
        //        .append("password", "admin");
            coll.insertOne(doc);
    }

    public static void insertNewDocument(org.managment.system.managmentsystem.mongodb.model.Document document){
        MongoCollection coll = connection.getCollection("documents");
       /* Document doc = new Document("name", document.getName()).append("version",document.getVersion()).append("date",document.getDate())
                .append("user",document.getUser()).append("changeJustification",document.getChangeJustification())
                .append("content",document.getContent()).append( "requests",document.getRequests());*/
        Document doc = new Document();

        doc.put("name",document.getName());
        doc.put("version",document.getVersion());
        doc.put("date",document.getDate());
        doc.put("user",document.getUser());
        doc.put("changeJustification",document.getChangeJustification());
        doc.put("notification",document.getNotification());
        doc.put("notificationDate",document.getNotificationDate());
        //doc.put("content",document.getContent());
        doc.put("requests",document.getRequests());
        doc.put("isNotificationShown",document.getIsNotificationShown());

        coll.insertOne(doc);
    }

    public static void insertUser(String username,String password,String companyName,String workEmail,String securityQuestion,String securityAnswer){
        MongoCollection coll = connection.getCollection("user");
        Document doc = new Document("username", username)
                .append("password", password)
                .append("companyName", companyName)
                .append("workEmail", workEmail)
                .append("securityQuestion", securityQuestion)
                .append("securityAnswer", securityAnswer);
            coll.insertOne(doc);
    }

}
