package org.managment.system.managmentsystem.view.registeruser;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;
import org.managment.system.managmentsystem.ManagmentSystemApplication;
import org.managment.system.managmentsystem.RequestWithDocumentController;
import org.managment.system.managmentsystem.mongodb.service.DBInserter;
import org.managment.system.managmentsystem.view.login.LoginController;

import java.io.IOException;

public class RegisterUserController {

    private Stage primaryStage;

    public RegisterUserController(Stage primaryStage){
        this.primaryStage = primaryStage;
    }

    @FXML
    TextField username,password,retypePassword,companyName,workEmail,securityQuestion,securityAnswer;
    @FXML
    ImageView checkPassword;
    private  Image image =null;


    @FXML
    public void onLoginClicked(){
        FXMLLoader fxmlLoader = new FXMLLoader(ManagmentSystemApplication.class.getResource("login.fxml"));
        fxmlLoader.setController(new LoginController(primaryStage));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 600, 400);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(400);
        primaryStage.setMaxWidth(600);
        primaryStage.setMaxHeight(400);
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(false);
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    @FXML
    public void onSaveButtonClick(){
        DBInserter.insertUser(username.getText(),password.getText(),companyName.getText(),workEmail.getText(),securityQuestion.getText(),securityAnswer.getText());
        onLoginClicked();
    }

    @FXML
    public void onKeyReleased(KeyEvent keyEvent){
        TextField rp = (TextField) keyEvent.getSource();

        if(password.getText().equals(rp.getText())){
            image = new Image(RequestWithDocumentController.class.getResource("images/ok.png").toExternalForm());
        }else{
            image = new Image(RequestWithDocumentController.class.getResource("images/error.png").toExternalForm());
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                checkPassword.setImage(image);
            }
        });

    }

}
