package org.managment.system.managmentsystem.mongodb.imports;

import org.managment.system.managmentsystem.mongodb.model.Request;
import org.managment.system.managmentsystem.mongodb.model.User;

import java.util.List;

public class RequestContainer {
    public List<Request> requests;

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }
}
