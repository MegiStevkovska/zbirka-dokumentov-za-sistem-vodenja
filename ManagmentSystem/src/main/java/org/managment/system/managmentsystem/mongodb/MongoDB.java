package org.managment.system.managmentsystem.mongodb;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.managment.system.managmentsystem.mongodb.model.Document;
import org.managment.system.managmentsystem.mongodb.service.DBFinder;

import java.util.List;

public class MongoDB {

    private static MongoDB databaseConnection = null;
    private final static String HOST = "localhost";
    private final static int PORT = 27017;

    private final static String databaseName ="ManagmentSystem";

    public static MongoDatabase connection;

    private MongoDB()
    {
        MongoClient mongoClient = new MongoClient(HOST, PORT);
        connection = mongoClient.getDatabase(databaseName);

    }

    public static MongoDB getInstance()
    {
        if (databaseConnection == null)
            databaseConnection = new MongoDB();
        DBFinder.listRequests();
        return databaseConnection;
    }
}
