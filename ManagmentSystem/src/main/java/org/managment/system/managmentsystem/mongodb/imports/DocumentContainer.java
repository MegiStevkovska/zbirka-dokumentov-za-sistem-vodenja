package org.managment.system.managmentsystem.mongodb.imports;

import org.managment.system.managmentsystem.mongodb.model.Document;


import java.util.List;

public class DocumentContainer {
    public List<Document> documents;

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
