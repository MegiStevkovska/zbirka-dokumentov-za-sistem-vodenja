package org.managment.system.managmentsystem.view.notification;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.managment.system.managmentsystem.RequestWithDocumentController;
import org.managment.system.managmentsystem.mongodb.model.Document;
import org.managment.system.managmentsystem.mongodb.service.DBFinder;
import org.managment.system.managmentsystem.mongodb.service.DBUpdater;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NotificationController {

    private Stage primaryStage;
    private String shortTitle;

    @FXML
    Label title,errorMessage;
    @FXML
    TextArea notification;
    @FXML
    DatePicker calendar;
    @FXML
    Button edit,save;
    @FXML
    TextField hh,mm;
    @FXML
    CheckBox isUserShow;

    private RequestWithDocumentController requestWithDocumentController;
    public NotificationController(Stage stage, String title1, RequestWithDocumentController requestWithDocumentController) {
        this.primaryStage = stage;
        this.shortTitle = title1;
        this.requestWithDocumentController = requestWithDocumentController;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                title.setText(title.getText() + title1);
              Document document = DBFinder.findDocumentByName(title1);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                LocalDate localDate = LocalDate.parse(document.getDate().split(" ")[0], formatter);
                hh.setText(document.getDate().split(" ")[1].split(":")[0]);
                mm.setText(document.getDate().split(" ")[1].split(":")[1]);
                calendar.setValue(localDate);
                notification.setText(document.getNotification());
                isUserShow.setSelected(Boolean.valueOf(document.getIsNotificationShown()));
                hh.textProperty().addListener( (observable, oldValue, newValue) -> {
if(!"".equals(newValue)){
                    if (isNumeric(newValue)) {
                        int i = Integer.valueOf(newValue.equals("") ? "1" : newValue);
                        if (i >= 0 && i < 25) {

                        } else {
                            newValue = oldValue;
                            hh.setText(oldValue);
                        }
                    } else {
                        newValue = oldValue;
                        hh.setText(oldValue);
                    }
                }
                });
                mm.textProperty().addListener( (observable, oldValue, newValue) -> {
                    if(!"".equals(newValue)){
                    if ("".equals(newValue) || isNumeric(newValue)) {
                        int i = Integer.valueOf(newValue.equals("") ? "1" : newValue);
                        if (i >=0 && i < 60) {

                        } else {
                            newValue = oldValue;
                            mm.setText(oldValue);
                        }
                    } else {
                        newValue = oldValue;
                        mm.setText(oldValue);
                    }
                }
                });
            }
        });
    }

    @FXML
    public void onSaveButtonClick(){
        if("".equals(hh.getText()))
            hh.setText("0");
        if("".equals(mm.getText()))
            mm.setText("0");
        edit.setDisable(false);
        save.setDisable(true);
        calendar.setDisable(true);
        notification.setDisable(true);
        hh.setDisable(true);
        mm.setDisable(true);
        DBUpdater.updateDocumentNotification(shortTitle,notification.getText());
        DBUpdater.updateDocumentNotificationDate(shortTitle, calendar.getValue().atTime(Integer.valueOf(hh.getText().equals("")?"0":hh.getText()), Integer.valueOf(mm.getText().equals("")?"0":mm.getText())));
    }

    @FXML
    public void onEditButtonClick(){
        edit.setDisable(true);
        save.setDisable(false);
        calendar.setDisable(false);
        notification.setDisable(false);
        hh.setDisable(false);
        mm.setDisable(false);
    }

    @FXML
    public void onCheckBoxClicked(){
        Document documetn =DBFinder.findDocumentByName(shortTitle);
        DBUpdater.updateDocumentIsNotificationShown(documetn.getName(),isUserShow.isSelected());
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
