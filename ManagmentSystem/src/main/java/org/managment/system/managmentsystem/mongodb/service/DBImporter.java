package org.managment.system.managmentsystem.mongodb.service;

import com.google.gson.Gson;
import org.managment.system.managmentsystem.mongodb.imports.DocumentContainer;
import org.managment.system.managmentsystem.mongodb.imports.RequestContainer;
import org.managment.system.managmentsystem.mongodb.imports.UsersContainer;
import org.managment.system.managmentsystem.mongodb.model.Document;
import org.managment.system.managmentsystem.mongodb.model.Request;
import org.managment.system.managmentsystem.mongodb.model.User;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class DBImporter {

     public static void importUsers(File userFile) throws IOException {
         Gson g = new Gson();
         String content = Files.readString(Path.of(userFile.getPath()), StandardCharsets.UTF_8);
         UsersContainer vc = g.fromJson(content, UsersContainer.class);
         List<User> userList = vc.getUsers();
         for(User user: userList){
             DBInserter.insertUser(user.getUserName(),user.getPassword(),user.getCompanyName(),user.getWorkEmail(),user.getSecurityQuestion(),user.getSecurityAnswer());
         }
    }

    public static void importRequests(File userFile) throws IOException {
        Gson g = new Gson();
        String content = Files.readString(Path.of(userFile.getPath()), StandardCharsets.UTF_8);
        RequestContainer rc = g.fromJson(content, RequestContainer.class);
        List<Request> requestList = rc.getRequests();
        for(Request request: requestList){
            DBInserter.insertNewRequest(request.getName());
        }
    }

    public static void importDocumnets(File userFile) throws IOException {
        Gson g = new Gson();
        String content = Files.readString(Path.of(userFile.getPath()), StandardCharsets.UTF_8);
        DocumentContainer dc = g.fromJson(content, DocumentContainer.class);
        List<Document> requestList = dc.getDocuments();
        for(Document document: requestList){
            DBInserter.insertNewDocument(document);
        }
    }
}
