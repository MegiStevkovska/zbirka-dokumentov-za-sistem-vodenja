package org.managment.system.managmentsystem.view.updatedocument;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.managment.system.managmentsystem.RequestWithDocumentController;
import org.managment.system.managmentsystem.enums.ChoserType;
import org.managment.system.managmentsystem.mongodb.model.Document;
import org.managment.system.managmentsystem.mongodb.service.DBInserter;

import java.io.File;

public class UpdateDocumentController {

    private Stage primaryStage;

    @FXML
    TextField fileName;
    @FXML
    Label title,errorMessage;
    @FXML
    TextArea changeJustification;
    private String shortTitle;
    private RequestWithDocumentController requestWithDocumentController;

    public UpdateDocumentController(Stage stage,String title1, RequestWithDocumentController requestWithDocumentController) {
        this.primaryStage = stage;
        this.shortTitle = title1;
        this.requestWithDocumentController = requestWithDocumentController;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                title.setText(title.getText() + title1);
            }
        });
    }

    @FXML
    public void onChosFileClick(){
        File file = createFielChoser(ChoserType.OPEN);
        if(file != null && !"".equals(file.getName())) {
            if(!file.getName().equals(shortTitle)){
                errorMessage.setText("File must have same name an type !");
            }else {
                fileName.setText(file.getName());
            }
        }
    }

    @FXML
    public void onSaveButtonClick(){
        if("".equals(fileName.getText().trim()) || "".equals(changeJustification.getText().trim())){
            errorMessage.setText("File name and Change justification are mandatory fild !");
        }else{
          requestWithDocumentController.update(changeJustification.getText(),fileName.getText());
        }

    }

    private File createFielChoser(ChoserType choserType){
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterTxt = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        FileChooser.ExtensionFilter extFilterXls = new FileChooser.ExtensionFilter("Excel 97-2003 (*.xls)", "*.xls");
        FileChooser.ExtensionFilter extFilterXlsx = new FileChooser.ExtensionFilter("Excel Workbook (*.xlsx)", "*.xlsx");
        FileChooser.ExtensionFilter extFilterDoc = new FileChooser.ExtensionFilter("Word 97-2003 Document (*.doc)", "*.doc");
        FileChooser.ExtensionFilter extFilterDocx = new FileChooser.ExtensionFilter("Word Document (*.docx)", "*.docx");
        FileChooser.ExtensionFilter extFilterPdf = new FileChooser.ExtensionFilter("PDF Files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilterTxt);
        fileChooser.getExtensionFilters().add(extFilterXls);
        fileChooser.getExtensionFilters().add(extFilterXlsx);
        fileChooser.getExtensionFilters().add(extFilterDoc);
        fileChooser.getExtensionFilters().add(extFilterDocx);
        fileChooser.getExtensionFilters().add(extFilterPdf);
        File f = null;
        switch (choserType){
            case SAVE -> f = fileChooser.showSaveDialog(primaryStage);
            default ->  f = fileChooser.showOpenDialog(primaryStage);
        }
        return  f;
    }
}
