package org.managment.system.managmentsystem.mongodb.adapter;



import com.google.gson.*;
import org.bson.json.JsonParseException;
import org.managment.system.managmentsystem.mongodb.model.Document;


import java.util.ArrayList;
import java.util.List;

public class DocumentsAdapter implements JsonDeserializer<Document> {
    public Document deserialize(JsonElement json,
                                java.lang.reflect.Type typeOfT, JsonDeserializationContext
                                        context) throws JsonParseException {
        JsonObject jObj = json.getAsJsonObject();
        //String id =
        //        jObj.get("_id").toString().replaceAll(".*\"(\\w+)\"}",
        //                "$1");
        String name = jObj.get("name") != null ?
                jObj.get("name").getAsString() : "";
        String version = jObj.get("version")!= null ?
                jObj.get("version").getAsString() : "";
        String date = jObj.get("date")!= null ?
                jObj.get("date").getAsString() : "";
        String user = jObj.get("user")!= null ?  jObj.get("user").getAsString() : "";
        String changeJustification = jObj.get("changeJustification")!= null ?  jObj.get("changeJustification").getAsString() : "";
        String content = jObj.get("content")!= null ?  jObj.get("content").getAsString() : "";
        String notification = jObj.get("notification")!= null ?  jObj.get("notification").getAsString() : "";
        String notificationDate = jObj.get("notificationDate")!= null ?  jObj.get("notificationDate").getAsString() : "";
        String[] requestArray = new Gson().fromJson(jObj.get("requests").getAsJsonArray(), String[].class);
        String isNotificationShown = jObj.get("isNotificationShown")!= null ?  jObj.get("isNotificationShown").getAsString() : "";

        List<String> requests =new ArrayList<String>();
  for(String s:requestArray){
      requests.add(s);
  }

        return new Document(name,version,date,user,changeJustification,content,notification,notificationDate,requests,isNotificationShown);
    }
}
