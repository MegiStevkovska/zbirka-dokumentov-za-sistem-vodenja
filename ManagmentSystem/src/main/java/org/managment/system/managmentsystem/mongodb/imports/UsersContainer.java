package org.managment.system.managmentsystem.mongodb.imports;

import org.managment.system.managmentsystem.mongodb.model.User;

import java.util.List;

public class UsersContainer {
    public List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
