package org.managment.system.managmentsystem.mongodb.model;

import java.util.List;
import java.util.Map;

public class Document {

    public Document(String name, String version, String date, String user, String changeJustification, String content, String notification, String notificationDate, List<String> requests,String isNotificationShown) {
        this.name = name;
        this.version = version;
        this.date = date;
        this.user = user;
        this.changeJustification = changeJustification;
        this.content = content;
        this.notification = notification;
        this.notificationDate = notificationDate;
        this.requests = requests;
        this.isNotificationShown = isNotificationShown;
    }

    private String name;
    private String version;
    private String date;
    private String user;
    private String changeJustification;
    private String content;

    private String notification;

    private String notificationDate;
    private String isNotificationShown;

    private List<String> requests;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getChangeJustification() {
        return changeJustification;
    }

    public void setChangeJustification(String changeJustification) {
        this.changeJustification = changeJustification;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getRequests() {
        return requests;
    }

    public void setRequests(List<String> requests) {
        this.requests = requests;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getIsNotificationShown() {
        return isNotificationShown;
    }

    public void setIsNotificationShown(String isNotificationShown) {
        this.isNotificationShown = isNotificationShown;
    }
}
